from django.contrib import admin
from .models import ParamMaster, ParamDetails


# Register your models here.
# @admin.register(ParamDetails)
# class ParamDetailsAdmin(admin.ModelAdmin):
#     fields = ['name', 'is_active']

@admin.register(ParamMaster)
class ParamMasterAdmin(admin.ModelAdmin):
    fields = ['name', 'created_on', 'is_active']
    list_filter = ['is_active']
    search_fields = ['name']

@admin.register(ParamDetails)
class ParamDetailsAdmin(admin.ModelAdmin):
    list_display = ['name','param_id']
    list_filter = ['param_id']
    search_fields = ['name']

#admin.site.register(ParamMasterAdmin)
#admin.site.register(ParamDetails)



