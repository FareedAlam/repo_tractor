from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class ParamMaster(models.Model):
    name = models.CharField(max_length=50, unique=True)
    created_on = models.DateField(auto_now_add=True)
    updated_by = models.ForeignKey(User, related_name='updated_by', null=True, blank=True,
                                   on_delete=models.CASCADE)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = self.name.lower()
        return super(ParamMaster, self).save(*args, **kwargs)


class ParamDetails(models.Model):
    param_id = models.ForeignKey(ParamMaster, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, unique=True, )
    created_on = models.DateField(auto_now_add=True)
#    updated_by = models.ForeignKey(User, related_name='updated_by', null=True, blank=True, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = self.name.lower()
        return super(ParamDetails, self).save(*args, **kwargs)